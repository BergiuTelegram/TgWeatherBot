from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram import ParseMode
import pyowm
import logging
import configparser
import os


class WeatherBot(object):
    """This is our Weather Telegram Bot"""

    def __init__(self, tg_token, weather_token):
        """
        Constructor

        Constructs a WeatherBot object

        Parameters
        ----------
        tg_token : str
            The Telegram Bot Token
        weather_token : str
            The openweathermap Token
        """
        self._tg_token = tg_token
        self._weather_token = weather_token
        # create api objects
        self.owm = pyowm.OWM(self._weather_token)
        self.updater = Updater(token=self._tg_token)
        self.dispatcher = self.updater.dispatcher
        # logging
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        # add command handlers
        self.dispatcher.add_handler(CommandHandler('start', self.start_handler))
        self.dispatcher.add_handler(CommandHandler('help', self.help_handler))
        self.dispatcher.add_handler(CommandHandler('weather', self.weather_handler, pass_args=True))

    def start_polling(self):
        """
        Starts polling messages from Telegram

        This method stats a background thread, that polls messages received by the telegram bot
        and handles the commands by sending answers.
        """
        self.updater.start_polling()

    def start_handler(self, bot, update):
        """
        Handler for the /start command

        This method handles the /start command and sends a welcome message.

        Parameters
        ----------
        bot :
            The telegram bot object
        update :
            The update message object
        """
        bot.send_message(chat_id=update.message.chat_id, text="Welcome to our WeatherBot. See /help for more information")

    def help_handler(self, bot, update):
        """
        Handler for the /help command

        This method handles the /help command and sends a help message.

        Parameters
        ----------
        bot :
            The telegram bot object
        update :
            The update message object
        """
        msg = "*Commands*"
        msg += "\n/help display the help message"
        msg += "\n/weather show the weather"
        msg += "\n\n*About*"
        msg += "\nThis bot is our product of the [TheVillage](https://t.me/theVillageChats)-Hackaton from 10.12.2017."
        msg += "\nIt's source is available under the MIT license on [gitlab](https://gitlab.com/BergiuTelegram/TgWeatherBot)"
        bot.send_message(chat_id=update.message.chat_id, text=msg, parse_mode=ParseMode.MARKDOWN)

    def weather_handler(self, bot, update, args):
        """
        Handler for the /weather command

        This method handles the /weather command and sends the weather for a given town or an error message

        Parameters
        ----------
        bot :
            The telegram bot object
        update :
            The update message object
        args :
            Arguments passed after the /weather command. Should be the town name.
        """
        if len(args) > 0:
            place = ' '.join(args)
            try:
                w = self.owm.weather_at_place(place).get_weather()
                status = w.get_status()
                wind = w.get_wind()
                humi = w.get_humidity()
                temp = w.get_temperature('celsius')
                msg = "The weather at " + place
                # the following is not documented and only based on what we could find out by our own
                if status == "Clouds":
                    msg += "\nStatus: " + status + " ☁️"
                elif status == "Sun":
                    msg += "\nStatus: " + status + " ☀️"
                elif status == "Rain":
                    msg += "\nStatus: " + status + " 🌧"
                elif status == "Snow":
                    msg += "\nStatus: " + status + " ☃️"
                elif status == "Clear":
                    msg += "\nStatus: " + status + " ☀️"
                elif status == "Storm":
                    msg += "\nStatus: " + status + " ⛈"
                else:
                    msg += "\nStatus: " + status + " 🌤"
                msg += "\n\n🌪 Wind: " + str(wind['speed']) + "km/h"
                msg += "\n💨 Humidity: " + str(humi) + "%"
                msg += "\n\n🌡Temperature: " + str(temp['temp']) + "℃"
                msg += "\n\t📉 Min: " + str(temp['temp_min']) + "℃"
                msg += "\n\t📈 Max: " + str(temp['temp_max']) + "℃"
                bot.send_message(chat_id=update.message.chat_id, text=msg)
            except pyowm.exceptions.not_found_error.NotFoundError:
                text = "Place " + place + " not found. Type a valid place, like this Example"
                text += "\n/weather cologne"
                bot.send_message(chat_id=update.message.chat_id, text=text)
        else:
            text = "You must provide a place where to lookup the weather. For Example:"
            text += "\n/weather cologne"
            bot.send_message(chat_id=update.message.chat_id, text=text)


def write_config(config_file_path, telegram_token, weather_token):
    """
    Write the tokens into the config file

    Writes the telegram_token and the weather_token into an config file.

    Parameters
    ----------
    config_file_path : str
        The path to the config file
    telegram_token : str
        The Telegram bot token
    weather_token : str
        The openweathermap token
    """
    config = configparser.ConfigParser()
    config['bot-config'] = {'telegram_token': telegram_token, 'weather_token': weather_token}
    with open(config_file_path, 'w') as configfile:
        config.write(configfile)


def create_config(config_file_path):
    """
    Asks the user for the tokens and saves them into a file

    Parameters
    ----------
    config_file_path : str
        The path to the config file
    """
    print("First you have to config this bot.")
    print("Create a Telegram Bot at @BotFather and copy the Token")
    telegram_token = input("Telegram Token: ")
    print("Login to https://openweathermap.org/api and create a Wether Token")
    weather_token = input("Weather Token: ")
    write_config(config_file_path, telegram_token, weather_token)


def main(config_file_path):
    """
    Main method

    Parameters
    ----------
    config_file_path : str
        The path to the config file
    """
    if os.path.isfile(config_file_path):
        try:
            configParser = configparser.RawConfigParser()
            configParser.read(config_file_path)
            telegram_token = configParser.get('bot-config', 'telegram_token')
            weather_token = configParser.get('bot-config', 'weather_token')
            weatherBot = WeatherBot(telegram_token, weather_token)
            print("Starte Bot")
            weatherBot.start_polling()
            print("Finished starting Bot")
        except configparser.NoOptionError:
            create_config(config_file_path)
            print("Now restart the program")
    else:
        create_config(config_file_path)
        print("Now restart the program")


if __name__ == "__main__":
    main('config.ini')
