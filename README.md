# Telegram Weather Bot

This bot is our product of the TheVillage-Hackaton from 10.12.2017 initiated by the awesome telegram chat network called [@TheVillageChats](https://t.me/TheVillageChats).

## Dependencies:
- [PyOwn](https://github.com/csparpa/pyowm)
	- `$ pip install git+https://github.com/csparpa/pyowm.git@develop`
- [Python-Telegram-Bot](https://github.com/python-telegram-bot/python-telegram-bot)
	- `$ pip install python-telegram-bot --upgrade`

## Instructions:
- Create a telegram bot at [@botfather](https://t.me/botfather)
- Login to [OpenWeatherMap](http://openweathermap.org/) and create a token
- Execute the bot and insert your tokens:
	- `python3 weatherbot.py`
- Execute the bot again to run it

## Commands
- `/help` display the help message
- `/weather $town` shows detailed information about the weather in $town

## Contributors
- [@GeloMyrtol](https://t.me/gelomyrtol)
- [@ulpa_h](https://t.me/ulpa_h)

## License
The source is available under the MIT license on [gitlab](https://gitlab.com/Bergiu/TgWeatherBot)
